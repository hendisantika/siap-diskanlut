# Siap Diskanlut

Sistem Informasi Penggajian (SIAP) Dinas Perikanan dan Kelautan (Diskanlut) Provinsi Jawa Barat

Untuk menjalankan projek ini silahkan copy paste perintah ini ke terminal anda :

`mvn clean spring-boot:run`

HEROKU LINK --> https://siapdiskanlut.herokuapp.com

### Image Screen shot

Login Page

![Login Page](img/login.png "Login Page")

Dashboard Page

![Dashboard Page](img/dashboard.png "Dashboard Page")

Bidang Page

![Bidang Page](img/bidang.png "Bidang Page")

![Bidang Page](img/bidang2.png "Bidang Page")

Gaji Page

![Gaji Page](img/input-gaji.png "Gaji Page")

![Input Gaji Pegawai](img/input-gaji.png "Input Gaji Pegawai")

Jabatan Page

![Jabatan Page](img/jabatan.png "Jabatan Page)

![Jabatan Page](img/jabatan2.png "Jabatan Page)

Pagu Page

![Pagu Page](img/pagu.png "Pagu Page")

![Pagu Page](img/pagu2.png "Pagu Page")

Pegawai Page

![Pegawai Page](img/pegawai.png "Pegawai Page")

![Pegawai Page](img/pegawai2.png "Pegawai Page")

SK Page

![SK Page](img/sk.png "SK Page")

![SK Page](img/sk2.png "SK Page")

Laporan Keuangan Page

![Laporan Keuangan Page](img/laporan-keuangan.png "Laporan Keuangan Page")

Laporan Fisik Page

![Laporan Fisik Page](img/fisik.png "Laporan Fisik Page")