package com.dewipurnamasari.diskanlut.siap.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/09/18
 * Time: 18.16
 * To change this template use File | Settings | File Templates.
 */

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "laporan_keuangan")
@Data
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Keuangan implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bidang_id", referencedColumnName = "id")
    private Bidang bidang;

    private BigDecimal t1;

    private BigDecimal t2;

    private BigDecimal t3;

    private BigDecimal t4;

    private BigDecimal t5;

    private BigDecimal t6;

    private BigDecimal t7;

    private BigDecimal t8;

    private BigDecimal t9;

    private BigDecimal t10;

    private BigDecimal t11;

    private BigDecimal t12;

    private BigDecimal r1;

    private BigDecimal r2;

    private BigDecimal r3;

    private BigDecimal r4;

    private BigDecimal r5;

    private BigDecimal r6;

    private BigDecimal r7;

    private BigDecimal r8;

    private BigDecimal r9;

    private BigDecimal r10;

    private BigDecimal r11;

    private BigDecimal r12;

    @CreatedDate
    @Column(nullable = false, updatable = false)
    private Date created;

    @LastModifiedDate
    @Column(nullable = false)
    private Date modified;


}
