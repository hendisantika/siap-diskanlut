package com.dewipurnamasari.diskanlut.siap.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 30/07/18
 * Time: 11.51
 * To change this template use File | Settings | File Templates.
 */

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Bidang {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String kdBidang;

    private String namaBidang;
}
