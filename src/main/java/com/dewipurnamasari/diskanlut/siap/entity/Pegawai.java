package com.dewipurnamasari.diskanlut.siap.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : siap
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/02/18
 * Time: 21.36
 * To change this template use File | Settings | File Templates.
 */

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Pegawai {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private Long pegawaiId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bidang_id", referencedColumnName = "id")
    private Bidang bidang;

    private String nama;

    private String tempatLahir;

    private String usia;

    @Temporal(value = TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date tglLahir;

    private String sex;

    private String noTelpon;

    private String statusMerital;

    private Integer jmlAnak;

    private String alamat;

    private String pendidikan;

    private String agama;

    private String namaSekolah;

    private String jurusan;

    private int tahunLulus;

    private String jabatan;

    @Temporal(value = TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date awalKerja;

    @Temporal(value = TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date akhirKerja;

    private String masaKerja;


}
