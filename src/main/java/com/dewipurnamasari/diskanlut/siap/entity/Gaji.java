package com.dewipurnamasari.diskanlut.siap.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/07/18
 * Time: 19.42
 * To change this template use File | Settings | File Templates.
 */

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Gaji implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String gajiId;

    private String nama;

    private Long noRekening;

    private String namaBank;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bidang_id", referencedColumnName = "id")
    private Bidang bidang;

    private int bulan;

    private Double jumlah;

    private Double bpjsTk;

    private Double bpjsKs;

    private Double pph;

    private Double total;

    private String terbilang;

    private String tempat;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "kpa_id")
    private Jabatan kpa;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bendahara_id")
    private Jabatan bendahara;

    private String ygMenerima;

    @CreatedDate
    @Column(nullable = false, updatable = false)
    private Date createdDate;
}
