package com.dewipurnamasari.diskanlut.siap.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/07/18
 * Time: 06.57
 * To change this template use File | Settings | File Templates.
 */

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Jabatan implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String jabatanId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bidang_id", referencedColumnName = "id")
    private Bidang bidang;

    private String nip;

    private String nama;

    private String pangkat;

    private String golongan;

    private String struktural;

    private String fungsional;

    @CreatedDate
    private Date createdDate;

}
