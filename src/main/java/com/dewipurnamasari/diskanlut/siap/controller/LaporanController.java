package com.dewipurnamasari.diskanlut.siap.controller;

import com.dewipurnamasari.diskanlut.siap.dto.KeuanganForm;
import com.dewipurnamasari.diskanlut.siap.entity.Fisik;
import com.dewipurnamasari.diskanlut.siap.entity.Keuangan;
import com.dewipurnamasari.diskanlut.siap.service.BidangService;
import com.dewipurnamasari.diskanlut.siap.service.FisikService;
import com.dewipurnamasari.diskanlut.siap.service.KeuanganService;
import com.dewipurnamasari.diskanlut.siap.service.PaguService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/09/18
 * Time: 20.17
 * To change this template use File | Settings | File Templates.
 */

@Controller
@RequestMapping("laporan")
@Slf4j
public class LaporanController {

    @Autowired
    PaguService paguService;

    @Autowired
    BidangService bidangService;

    @Autowired
    KeuanganService keuanganService;

    @Autowired
    FisikService fisikService;

    @GetMapping("/keuangan")
    String showKeuanganForm(Model model) {
        model.addAttribute("keuangan", new KeuanganForm());
        model.addAttribute("listBidang", bidangService.findAll());
        return "laporan/keuangan";
    }


    @GetMapping("/fisik")
    String showFisikForm(Model model) {
        model.addAttribute("fisik", new KeuanganForm());
        model.addAttribute("listBidang", bidangService.findAll());
        return "laporan/fisik";
    }

    @PostMapping("/keuangan")
    public String saveKeuangan(KeuanganForm keuanganForm, BindingResult result) {
        if (result.hasErrors()) {
            log.debug("Data! --> " + keuanganForm);
            log.debug("There are errors! --> " + result);
            return "gaji/list";
        }

        Keuangan entity = new Keuangan();
        BeanUtils.copyProperties(keuanganForm, entity);
        log.info("Keuangan Form --> " + keuanganForm);
        log.info("Keuangan Entity --> " + entity);
        keuanganService.save(entity);

        return "redirect:keuangan";
    }

    @PostMapping("/fisik")
    public String saveLaporanFisik(KeuanganForm keuanganForm, BindingResult result) {
        if (result.hasErrors()) {
            log.debug("Data Keuangan --> " + keuanganForm);
            log.debug("There are errors! --> " + result);
            return "gaji/list";
        }

        Fisik entity = new Fisik();
        BeanUtils.copyProperties(keuanganForm, entity);
        log.info("Fisik Form --> " + keuanganForm);
        log.info("Fisik Entity --> " + entity);
        fisikService.save(entity);

        return "redirect:fisik";
    }
}
