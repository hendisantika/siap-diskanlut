package com.dewipurnamasari.diskanlut.siap.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/08/18
 * Time: 00.06
 * To change this template use File | Settings | File Templates.
 */
@RequestMapping
@Controller
public class HomeController {

    private static final Logger logger = LogManager.getLogger(AdminController.class);

    @RequestMapping(value = {"/", "/signin"})
    public String signin() {

        return "signin";
    }


    @RequestMapping(value = "/error/404")
    public String error404() {

        return "error/404";
    }

    @RequestMapping(value = "/error/505")
    public String error505() {

        return "error/505";
    }

    @GetMapping("/tes")
    public String tes() {
        return "index";
    }
}
