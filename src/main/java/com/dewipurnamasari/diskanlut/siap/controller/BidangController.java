package com.dewipurnamasari.diskanlut.siap.controller;

import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import com.dewipurnamasari.diskanlut.siap.service.BidangService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/08/18
 * Time: 13.07
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
@Controller
@RequestMapping("bidang")
public class BidangController {
    @Autowired
    BidangService bidangService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true, 10));
    }

    @GetMapping("/list")
    public ModelAndView findAll() {

        ModelAndView mv = new ModelAndView("bidang/listBidang");
        mv.addObject("bidang", bidangService.findAll());

        return mv;
    }

    @GetMapping("/add")
    public String add(Model model) {

        model.addAttribute("bidang", new Bidang());
        model.addAttribute("allBidang", bidangService.findAll());
        model.addAttribute("title", "Input");

        return "bidang/input";
    }


    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        model.addAttribute("bidang", bidangService.findOne(id));
        model.addAttribute("allBidang", bidangService.findAll());
        model.addAttribute("title", "Edit");
        return "bidang/editBidang";
    }

    @GetMapping("/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {

        bidangService.delete(id);

        return findAll();
    }

    @PostMapping("/save")
    public String save(@Valid Bidang bidang, Model model, BindingResult result) {
        if (result.hasErrors()) {
            log.debug("Data! --> " + bidang);
            log.debug("There are errors! --> " + result);
            return "redirect:bidang/input";
        }

        bidangService.save(bidang);
        log.info("Data -> " + bidang);
        log.info("Save Data Sukses!");

        return "redirect:/bidang/add";
    }
}
