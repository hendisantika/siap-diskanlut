package com.dewipurnamasari.diskanlut.siap.controller;

import com.dewipurnamasari.diskanlut.siap.dto.PaguDTO;
import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import com.dewipurnamasari.diskanlut.siap.entity.Pagu;
import com.dewipurnamasari.diskanlut.siap.service.BidangService;
import com.dewipurnamasari.diskanlut.siap.service.PaguService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : siap
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/02/18
 * Time: 13.07
 * To change this template use File | Settings | File Templates.
 */

@Controller
@RequestMapping("/pagu")
public class PaguController {

    private static final Logger log = LogManager.getLogger(PaguController.class);

    @Autowired
    PaguService paguService;

    @Autowired
    BidangService bidangService;

    @GetMapping("/list")
    public ModelAndView findAll() {

        ModelAndView mv = new ModelAndView("pagu/listPagu");
        mv.addObject("pagu", paguService.findAll());

        return mv;
    }

    @GetMapping("/add")
    public String addPagu(Model model) {
        List<Bidang> listBidang = bidangService.findAll();

        model.addAttribute("pagu", new Pagu());
        model.addAttribute("listBidang", listBidang);

        return "pagu/inputPagu";
    }

    @GetMapping("/edit")
    public ModelAndView editPagu(@Valid Pagu pagu) {
        List<Bidang> listBidang = bidangService.findAll();
        ModelAndView mv = new ModelAndView("pagu/editPagu");
        mv.addObject("pagu", pagu);
        mv.addObject("listBidang", listBidang);

        return mv;
    }
//
//    @GetMapping("/edit/{id}")
//    public ModelAndView editPagu(@PathVariable("id") String id) {
//        return editPagu(paguService.findOne(id));
//    }


    @GetMapping("/edit/{id}")
    public String editPagu(@PathVariable("id") String id, Model model) {
        List<Bidang> listBidang = bidangService.findAll();
        model.addAttribute("pagu", paguService.findOne(id));
        model.addAttribute("listBidang", listBidang);

        return "pagu/editPagu";
    }

    @GetMapping("/delete/{id}")
    public ModelAndView deletePagu(@PathVariable("id") String id) {

        paguService.delete(id);

        return findAll();
    }

    @PostMapping("/save")
    public String savePagu(@Valid PaguDTO paguDTO, BindingResult result) {
        if (result.hasErrors()) {
            log.debug("Data! --> " + paguDTO);
            log.debug("There are errors! --> " + result);
            return "redirect:/pagu/list";
        }

        paguService.save(paguDTO);
        log.info("Data -> " + paguDTO);
        log.info("Save Data Sukses!");


        return "redirect:/pagu/list";
    }
}
