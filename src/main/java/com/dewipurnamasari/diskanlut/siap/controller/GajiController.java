package com.dewipurnamasari.diskanlut.siap.controller;

import com.dewipurnamasari.diskanlut.siap.dto.GajiForm;
import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import com.dewipurnamasari.diskanlut.siap.entity.Gaji;
import com.dewipurnamasari.diskanlut.siap.entity.Jabatan;
import com.dewipurnamasari.diskanlut.siap.service.BidangService;
import com.dewipurnamasari.diskanlut.siap.service.GajiService;
import com.dewipurnamasari.diskanlut.siap.service.JabatanService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/08/18
 * Time: 17.25
 * To change this template use File | Settings | File Templates.
 */

@Controller
@RequestMapping("gaji")
public class GajiController {
    private Logger log = LogManager.getLogger(GajiController.class);

    @Autowired
    JabatanService jabatanService;

    @Autowired
    BidangService bidangService;

    @Autowired
    GajiService gajiService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true, 10));
    }

    @GetMapping("/list")
    public String findAll(Model model, @RequestParam(value = "bulan", required = false) Integer bulan, @RequestParam(value = "bidang", required = false) Integer bidang) {
        List<Bidang> listBidang = bidangService.findAll();
        List<Gaji> result = null;

        if ((bulan != null) && (bidang != null)) {
            result = gajiService.findByBulanAndBidang(bulan, bidang);
        } else {
            result = gajiService.findAll();
        }
        model.addAttribute("listBidang", listBidang);
        model.addAttribute("gaji", result);

        return "gaji/filter";
    }


    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, Model model) {
        List<Bidang> listBidang = bidangService.findAll();
        List<Jabatan> listKpa = jabatanService.findKpa("KPA");
        List<Jabatan> listBdhr = jabatanService.findKpa("Ben");
        model.addAttribute("title", "Edit");
        model.addAttribute("listBidang", listBidang);
        model.addAttribute("gaji", gajiService.findOne(id));
        model.addAttribute("allBidang", bidangService.findAll());
        model.addAttribute("listKpa", listKpa);
        model.addAttribute("listBdhr", listBdhr);

        return "gaji/editGaji";
    }

    @GetMapping("/view/{id}")
    public String view(@PathVariable("id") String id, Model model) {
        List<Bidang> listBidang = bidangService.findAll();
        List<Jabatan> listKpa = jabatanService.findKpa("KPA");
        List<Jabatan> listBdhr = jabatanService.findKpa("Ben");
        model.addAttribute("title", "View");
        model.addAttribute("listBidang", listBidang);
        model.addAttribute("gaji", gajiService.findOne(id));
        model.addAttribute("allBidang", bidangService.findAll());
        model.addAttribute("listKpa", listKpa);
        model.addAttribute("listBdhr", listBdhr);

        return "gaji/viewGaji";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") String id, Model model) {
        gajiService.delete(id);
        model.addAttribute("gaji", gajiService.findAll());
        return "gaji/listGaji";
    }

    @GetMapping("/add")
    public String add(Model model) {

        model.addAttribute("gaji", new Gaji());
        model.addAttribute("gaji2", new GajiForm());
        model.addAttribute("title", "Input");

        List<Bidang> listBidang = bidangService.findAll();
        model.addAttribute("listBidang", listBidang);

        List<Jabatan> listKpa = jabatanService.findKpa("KPA");
        List<Jabatan> listBdhr = jabatanService.findKpa("Ben");
        model.addAttribute("listKpa", listKpa);
        model.addAttribute("listBdhr", listBdhr);

        return "gaji/inputGaji";
    }

    @PostMapping("/save")
    public String save(GajiForm gajiForm, BindingResult result) {
        if (result.hasErrors()) {
            log.debug("Data! --> " + gajiForm);
            log.debug("There are errors! --> " + result);
            return "gaji/list";
        }
        log.info("Gaji Form --> " + gajiForm);
        log.info("Save Data Sukses!");
        gajiService.save(gajiForm);

        return "redirect:list";
    }


}
