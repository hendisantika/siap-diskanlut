package com.dewipurnamasari.diskanlut.siap.controller;

import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import com.dewipurnamasari.diskanlut.siap.entity.Pegawai;
import com.dewipurnamasari.diskanlut.siap.service.BidangService;
import com.dewipurnamasari.diskanlut.siap.service.PegawaiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : siap
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/02/18
 * Time: 21.48
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
@Controller
@RequestMapping("pegawai")
public class PegawaiController {
    @Autowired
    PegawaiService pegawaiService;

    @Autowired
    BidangService bidangService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true, 10));
    }

    @GetMapping("/list")
    public ModelAndView findAll() {

        ModelAndView mv = new ModelAndView("pegawai/listPegawai");
        mv.addObject("pegawai", pegawaiService.findAll());

        return mv;
    }

    @GetMapping("/add")
    public String addPegawai(Model model) {

        model.addAttribute("pegawai", new Pegawai());
        model.addAttribute("title", "Input");
        List<Bidang> listBidang = bidangService.findAll();
        model.addAttribute("listBidang", listBidang);

        return "pegawai/input";
    }


    @GetMapping("/{id}")
    public String editPegawai(@RequestParam("aksi") String aksi, @PathVariable("id") String id, Model model) {
        List<Bidang> listBidang = bidangService.findAll();
        model.addAttribute("pegawai", pegawaiService.findOne(id));
        model.addAttribute("listBidang", listBidang);
        model.addAttribute("title", "Edit");
        log.info("Data Pegawai --> " + pegawaiService.findOne(id));
        log.info("Data Bidang --> " + listBidang);
        if (aksi.equals("edit")) {
            return "pegawai/input";
        } else {
            return "pegawai/viewPegawai";

        }
    }


    @GetMapping("/delete/{id}")
    public ModelAndView deletePegawai(@PathVariable("id") String id) {

        pegawaiService.delete(id);

        return findAll();
    }

    @PostMapping("/save")
    public String savePegawai(@Valid Pegawai pegawai, Model model, BindingResult result) {
        if (result.hasErrors()) {
            log.debug("Data! --> " + pegawai);
            log.debug("There are errors! --> " + result);
            return "redirect:/pegawai/list";
        }

        pegawaiService.save(pegawai);
        log.info("Data -> " + pegawai);
        log.info("Save Data Sukses!");


        return "redirect:/pegawai/list";
    }
}
