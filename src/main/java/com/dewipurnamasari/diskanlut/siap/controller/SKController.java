package com.dewipurnamasari.diskanlut.siap.controller;

import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import com.dewipurnamasari.diskanlut.siap.entity.SK;
import com.dewipurnamasari.diskanlut.siap.service.BidangService;
import com.dewipurnamasari.diskanlut.siap.service.SKService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/07/18
 * Time: 17.30
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
@Controller
@RequestMapping("/sk")
public class SKController {

    @Autowired
    SKService skService;

    @Autowired
    BidangService bidangService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true, 10));
    }

    @GetMapping("/list")
    public ModelAndView findAll() {

        ModelAndView mv = new ModelAndView("sk/listSK");
        mv.addObject("sk", skService.findAll());

        return mv;
    }

    @GetMapping("/add")
    public ModelAndView add(SK sk) {

        List<Bidang> listBidang = bidangService.findAll();
        ModelAndView mv = new ModelAndView("sk/inputSK");
        mv.addObject("sk", sk);
        mv.addObject("listBidang", listBidang);

        return mv;
    }


    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, Model model) {
        List<Bidang> listBidang = bidangService.findAll();
        model.addAttribute("sk", skService.findOne(id));
        model.addAttribute("listBidang", listBidang);
        model.addAttribute("title", "Edit");

        log.info("allBidang --> " + listBidang);
        log.info("sk --> " + model.toString());
        return "sk/editSK";
    }

    @GetMapping("/delete/{id}")
    public ModelAndView delete(@PathVariable("id") String id) {

        skService.delete(id);
        return findAll();
    }

    @PostMapping("/save")
    public ModelAndView save(@Valid SK sk, BindingResult result) {
        if (result.hasErrors()) {
            log.debug("Data! --> " + sk);
            log.debug("There are errors! --> " + result);
            return add(sk);
        }

        skService.save(sk);
        log.info("Data -> " + sk);
        log.info("Save Data Sukses!");


        return findAll();
    }
}
