package com.dewipurnamasari.diskanlut.siap.controller;

import com.dewipurnamasari.diskanlut.siap.dto.RealisasiDTO;
import com.dewipurnamasari.diskanlut.siap.entity.*;
import com.dewipurnamasari.diskanlut.siap.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 03/10/18
 * Time: 18.14
 * To change this template use File | Settings | File Templates.
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class ApiController {
    @Autowired
    PegawaiService pegawaiService;

    @Autowired
    PaguService paguService;

    @Autowired
    KeuanganService keuanganService;

    @Autowired
    FisikService fisikService;

    @Autowired
    GajiService gajiService;

    @GetMapping("/pagu/{id}")
    Pagu getPagu(@PathVariable(value = "id") long id) {
        Bidang bidang = new Bidang();
        bidang.setId(id);
        log.info("Pagu --> ", paguService.findById(bidang));
        return paguService.findById(bidang);
    }

    @GetMapping("/laporan/keuangan/{id}")
    Keuangan getLaporanKeuanganByBidangID(@PathVariable(value = "id") long id) {
        Bidang bidang = new Bidang();
        bidang.setId(id);
        log.info("Data Laporan Keuangan --> ", keuanganService.findById(bidang));
        return keuanganService.findById(bidang);
    }

    @GetMapping("/laporan/fisik/{id}")
    Fisik getLaporanFisikByBidangID(@PathVariable(value = "id") long id) {
        Bidang bidang = new Bidang();
        bidang.setId(id);
        log.info("Data Laporan Fisik --> ", fisikService.findById(bidang));
        return fisikService.findById(bidang);
    }

    @GetMapping("/laporan/fisikId/{id}")
    Fisik getLaporanFisikIDByBidangID(@PathVariable(value = "id") long id) {
        return fisikService.getLapFisikIDById(id);
    }


    @GetMapping("/gaji/realisasi/{id}")
    List<RealisasiDTO> getRealisasiByBidang(@PathVariable(value = "id") int id) {
        List<RealisasiDTO> data = gajiService.getRealisasiByBidang(id);
        log.info("Data Realisasi perbidang --> {}", data);
        return data;
    }

    @GetMapping("/pegawai")
    @ResponseBody
    public List<Pegawai> getAllPegawai() {
        return pegawaiService.findAll();
    }

    @GetMapping("/pegawai/search")
    @ResponseBody
    public List<String> search(HttpServletRequest request) {
        return pegawaiService.search(request.getParameter("term"));
    }

    @GetMapping("/gaji")
    public List<Gaji> listGaji() {
        return gajiService.findAll();
    }
}
