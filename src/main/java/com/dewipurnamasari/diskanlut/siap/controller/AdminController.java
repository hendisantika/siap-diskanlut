package com.dewipurnamasari.diskanlut.siap.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 24/07/18
 * Time: 17.30
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class AdminController {

    private static final Logger logger = LogManager.getLogger(AdminController.class);

    @RequestMapping(value = "/admin/dashboard.html")
    public String dashboard() {

        return "dashboard";
    }

    @GetMapping("/error/401")
    public String unauthorized() {
        return "error/401";
    }

    @GetMapping("/error/404")
    public String notFound() {
        return "error/404";
    }

    @GetMapping("/error/500")
    public String accessDenied() {
        return "error/500";
    }
}
