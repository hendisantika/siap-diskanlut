package com.dewipurnamasari.diskanlut.siap.controller;

import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import com.dewipurnamasari.diskanlut.siap.entity.Jabatan;
import com.dewipurnamasari.diskanlut.siap.service.BidangService;
import com.dewipurnamasari.diskanlut.siap.service.JabatanService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/07/18
 * Time: 07.41
 * To change this template use File | Settings | File Templates.
 */

@Controller
@RequestMapping("jabatan")
public class JabatanController {
    private static final Logger log = LogManager.getLogger(JabatanController.class);

    @Autowired
    JabatanService jabatanService;

    @Autowired
    BidangService bidangService;

    @GetMapping("/list")
    public String findAll(Model model) {

        model.addAttribute("jabatan", jabatanService.findAll());

        return "jabatan/listJabatan";
    }

    @GetMapping("/add")
    public String add(Model model) {

        List<Bidang> listBidang = bidangService.findAll();
        model.addAttribute("jabatan", new Jabatan());
        model.addAttribute("listBidang", listBidang);
        model.addAttribute("title", "Input");

        return "jabatan/inputJabatan";
    }

    @GetMapping("/edit")
    public ModelAndView editJabatan(Jabatan jabatan) {

        ModelAndView mv = new ModelAndView("jabatan/editJabatan");
        mv.addObject("jabatan", jabatan);
        List<Bidang> listBidang = bidangService.findAll();
        mv.addObject("listBidang", listBidang);
        mv.setViewName("/jabatan/listJabatan");

        return mv;
    }

    @GetMapping("/edit/{id}")
    public String editJabatan(@PathVariable("id") String id, Model model) {
        List<Bidang> listBidang = bidangService.findAll();
        model.addAttribute("listBidang", listBidang);
        model.addAttribute("jabatan", jabatanService.findOne(id));
        model.addAttribute("title", "Edit");

        log.info("allBidang --> " + listBidang);
        log.info("jabatan --> " + model.toString());
        return "jabatan/editJabatan";
    }


    @GetMapping("/delete/{id}")
    public String deleteJabatan(@PathVariable("id") Jabatan jabatan) {

        jabatanService.delete(jabatan);
        log.info("Delete Jabatan {}", jabatan);
        return "redirect:jabatan/listJabatan";
    }

    @PostMapping("/save")
    public String save(@Valid Jabatan jabatan, BindingResult result) {
        Long bidangId = null;
        bidangId = jabatan.getBidang() != null ? jabatan.getBidang().getId() : -1L;
        bidangId = bidangId == null ? -1 : bidangId;

        Optional<Bidang> bid = bidangService.findById(bidangId);
        jabatan.setBidang(bid.isPresent() ? bid.get() : null);
        if (result.hasErrors()) {
            log.debug("Data! --> " + jabatan);
            log.debug("There are errors! --> " + result);
            return "redirect:jabatan/inputJabatan";
        }

        jabatanService.save(jabatan);
        log.info("Data -> " + jabatan);
        log.info("Save Data Sukses!");

        return "redirect:/jabatan/list";
    }

}
