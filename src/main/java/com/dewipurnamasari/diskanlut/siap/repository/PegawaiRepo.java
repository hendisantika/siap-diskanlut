package com.dewipurnamasari.diskanlut.siap.repository;

import com.dewipurnamasari.diskanlut.siap.entity.Pegawai;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : siap
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 13/02/18
 * Time: 21.45
 * To change this template use File | Settings | File Templates.
 */

@Repository
public interface PegawaiRepo extends JpaRepository<Pegawai, String> {
    @Query("SELECT nama FROM Pegawai where nama like %:keyword%")
    List<String> search(@Param("keyword") String keyword);
}
