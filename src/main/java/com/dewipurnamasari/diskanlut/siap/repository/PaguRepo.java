package com.dewipurnamasari.diskanlut.siap.repository;

import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import com.dewipurnamasari.diskanlut.siap.entity.Pagu;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : siap
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 14/02/18
 * Time: 21.57
 * To change this template use File | Settings | File Templates.
 */
public interface PaguRepo extends JpaRepository<Pagu, String> {

    Pagu findByBidangIs(Bidang bidangId);
}
