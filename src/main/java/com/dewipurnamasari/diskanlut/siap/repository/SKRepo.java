package com.dewipurnamasari.diskanlut.siap.repository;

import com.dewipurnamasari.diskanlut.siap.entity.SK;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/07/18
 * Time: 17.26
 * To change this template use File | Settings | File Templates.
 */
public interface SKRepo extends JpaRepository<SK, String> {
}
