package com.dewipurnamasari.diskanlut.siap.repository;

import com.dewipurnamasari.diskanlut.siap.entity.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 24/07/18
 * Time: 16.03
 * To change this template use File | Settings | File Templates.
 */
@Repository
@Qualifier(value = "userRepository")
public interface UserRepository extends CrudRepository<User, String> {
    User findByUsername(String username);
}