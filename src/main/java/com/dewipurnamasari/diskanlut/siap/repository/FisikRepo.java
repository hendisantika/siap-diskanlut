package com.dewipurnamasari.diskanlut.siap.repository;

import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import com.dewipurnamasari.diskanlut.siap.entity.Fisik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/10/18
 * Time: 21.02
 * To change this template use File | Settings | File Templates.
 */
public interface FisikRepo extends JpaRepository<Fisik, String> {
    Fisik findByBidangIs(Bidang bidangId);

    @Query(value = "SELECT * FROM laporan_fisik WHERE bidang_id = :bidangId", nativeQuery = true)
    Fisik getLapFisikID(@Param("bidangId") long bidangId);
}
