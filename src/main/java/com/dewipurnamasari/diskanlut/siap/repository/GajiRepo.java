package com.dewipurnamasari.diskanlut.siap.repository;

import com.dewipurnamasari.diskanlut.siap.dto.RealisasiDTO;
import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import com.dewipurnamasari.diskanlut.siap.entity.Gaji;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/07/18
 * Time: 19.05
 * To change this template use File | Settings | File Templates.
 */
public interface GajiRepo extends PagingAndSortingRepository<Gaji, String> {

    @Query(value = "SELECT bidang_id, bulan, sum(jumlah) as total FROM gaji WHERE bidang_id = :bidangId GROUP BY bidang_id, bulan",
            nativeQuery = true)
    List<RealisasiDTO> getRealisasiByBidang(@Param("bidangId") int bidangId);

    @Query(value = "SELECT * from gaji", nativeQuery = true)
    List<Gaji> getRealisasi();

    List<Gaji> findByBulanAndBidangIs(int bulan, Bidang bidang);
}
