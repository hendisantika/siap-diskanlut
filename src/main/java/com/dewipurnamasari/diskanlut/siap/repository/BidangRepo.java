package com.dewipurnamasari.diskanlut.siap.repository;

import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/08/18
 * Time: 13.04
 * To change this template use File | Settings | File Templates.
 */
public interface BidangRepo extends JpaRepository<Bidang, Long> {

    Optional<Bidang> findById(Long id);
}
