package com.dewipurnamasari.diskanlut.siap.repository;

import com.dewipurnamasari.diskanlut.siap.entity.Jabatan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/07/18
 * Time: 07.05
 * To change this template use File | Settings | File Templates.
 */
public interface JabatanRepo extends JpaRepository<Jabatan, String> {

    List<Jabatan> findByFungsionalContaining(String kpa);
}
