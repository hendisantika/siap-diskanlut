package com.dewipurnamasari.diskanlut.siap.repository;

import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import com.dewipurnamasari.diskanlut.siap.entity.Keuangan;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/09/18
 * Time: 20.00
 * To change this template use File | Settings | File Templates.
 */
public interface KeuanganRepo extends JpaRepository<Keuangan, String> {
    Keuangan findByBidangIs(Bidang bidangId);
}
