package com.dewipurnamasari.diskanlut.siap.service;

import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import com.dewipurnamasari.diskanlut.siap.repository.BidangRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/08/18
 * Time: 13.04
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
public class BidangService {
    @Autowired
    BidangRepo bidangRepo;

    public List<Bidang> findAll() {
        return bidangRepo.findAll();
    }

    public Bidang findOne(Long id) {
        return bidangRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Bidang " + id + " Not found"));
    }

    public Bidang save(Bidang bidang) {
        return bidangRepo.saveAndFlush(bidang);
    }

    public void delete(Long id) {
        bidangRepo.deleteById(id);
    }

    public Optional<Bidang> findById(Long id) {
        return bidangRepo.findById(id);
    }

}
