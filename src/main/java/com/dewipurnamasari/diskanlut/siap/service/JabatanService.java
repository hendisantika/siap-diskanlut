package com.dewipurnamasari.diskanlut.siap.service;

import com.dewipurnamasari.diskanlut.siap.entity.Jabatan;
import com.dewipurnamasari.diskanlut.siap.repository.JabatanRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/07/18
 * Time: 07.06
 * To change this template use File | Settings | File Templates.
 */

@Service
@Transactional
@RequiredArgsConstructor
public class JabatanService {
    private final JabatanRepo jabatanRepo;

    public List<Jabatan> findAll() {
        return jabatanRepo.findAll();
    }

    public Jabatan findOne(String id) {
        return jabatanRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Jabatan with {} " + id + " Not found"));
    }

    public Jabatan save(Jabatan jabatan) {
        return jabatanRepo.saveAndFlush(jabatan);
    }

    public void delete(Jabatan jabatan) {
        jabatanRepo.delete(jabatan);
    }

    public List<Jabatan> findKpa(String kpa) {
        return jabatanRepo.findByFungsionalContaining(kpa);

    }
}
