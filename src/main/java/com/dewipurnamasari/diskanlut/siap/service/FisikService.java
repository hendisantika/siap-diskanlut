package com.dewipurnamasari.diskanlut.siap.service;

import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import com.dewipurnamasari.diskanlut.siap.entity.Fisik;
import com.dewipurnamasari.diskanlut.siap.repository.FisikRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 31/10/18
 * Time: 21.03
 * To change this template use File | Settings | File Templates.
 */

@Service
@Transactional
@Slf4j
public class FisikService {
    @Autowired
    FisikRepo fisikRepo;

    public List<Fisik> findAll() {
        return fisikRepo.findAll();
    }

    public Fisik findOne(String id) {
        return fisikRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Fisik " + id + " Not found"));
    }

    public Fisik save(Fisik fisik) {
        return fisikRepo.saveAndFlush(fisik);
    }

    public void delete(Fisik fisik) {
        fisikRepo.delete(fisik);
    }

    public Fisik findById(Bidang bidangId) {
        log.info("Bidang ID {}", bidangId);
        log.info("Data Laporan Fisik {}", bidangId);
        return fisikRepo.findByBidangIs(bidangId);
    }

    public Fisik getLapFisikIDById(long id) {
        Bidang bidang = new Bidang();
        bidang.setId(id);
        log.info("Bidang ID {}", bidang);
        log.info("Data Laporan Fisik ID {}", bidang);
        return fisikRepo.getLapFisikID(id);
    }
}
