package com.dewipurnamasari.diskanlut.siap.service;

import com.dewipurnamasari.diskanlut.siap.dto.PaguDTO;
import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import com.dewipurnamasari.diskanlut.siap.entity.Pagu;
import com.dewipurnamasari.diskanlut.siap.repository.PaguRepo;
import com.dewipurnamasari.diskanlut.siap.utils.DiskanlutUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : siap
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/02/18
 * Time: 13.08
 * To change this template use File | Settings | File Templates.
 */

@Transactional
@Service
@Slf4j
public class PaguService {
    @Autowired
    PaguRepo paguRepo;

    public List<Pagu> findAll() {
        return paguRepo.findAll();
    }

    public Pagu findOne(String id) {
        return paguRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Pagu " + id + " Not found"));
    }

    public Pagu findById(Bidang bidangId) {
        log.info("Bidang ID {}", bidangId);
        return paguRepo.findByBidangIs(bidangId);
    }

    public Pagu save(PaguDTO paguDTO) {
        Pagu pagu = new Pagu();
        pagu.setId(paguDTO.getId());
        pagu.setDpaId(paguDTO.getDpaId());
        pagu.setJmlAnggaran(DiskanlutUtils.convertToNumber(paguDTO.getJmlAnggaran()));
        pagu.setJmlBulan(paguDTO.getJmlBulan());
        pagu.setJmlOrang(paguDTO.getJmlOrang());
        pagu.setBidang(paguDTO.getBidang());
        pagu.setKdProgram(paguDTO.getKdProgram());
        pagu.setKdKegiatan(paguDTO.getKdKegiatan());
        pagu.setKdRekening(paguDTO.getKdRekening());

        return paguRepo.saveAndFlush(pagu);
    }

    public void delete(String id) {
        paguRepo.deleteById(id);
    }



}
