package com.dewipurnamasari.diskanlut.siap.service;

import com.dewipurnamasari.diskanlut.siap.entity.Pegawai;
import com.dewipurnamasari.diskanlut.siap.repository.PegawaiRepo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : siap
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/02/18
 * Time: 06.49
 * To change this template use File | Settings | File Templates.
 */

@Service
@Transactional
public class PegawaiService {

    private static final Logger log = LogManager.getLogger(PegawaiService.class);

    @Autowired
    PegawaiRepo pegawaiRepo;

    public List<Pegawai> findAll() {
        return pegawaiRepo.findAll();
    }

    public Pegawai findOne(String id) {
        return pegawaiRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Pegawai " + id + " Not found"));
    }

    public static String hitungUsia(Date tglLahir) {
        String usia = null;
        try {
            Date now = new Date();
            DateTime dateTime1 = new DateTime(tglLahir);
            DateTime dateTime2 = new DateTime(now);

            Period period = new Period(dateTime1, dateTime2);

            PeriodFormatter formatter = new PeriodFormatterBuilder()
                    .appendYears().appendSuffix(" tahun ")
                    .appendMonths().appendSuffix(" bulan ")
                    .appendWeeks().appendSuffix(" minggu ")
                    .appendDays().appendSuffix(" hari ")
                    .printZeroNever()
                    .toFormatter();

            String elapsed = formatter.print(period);

            usia = elapsed;

            log.info("Hasilnya : " + elapsed);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return usia;
    }

    public void delete(String id) {
        pegawaiRepo.deleteById(id);
    }

    public Pegawai save(Pegawai pegawai) {

        try {
            Date date1 = pegawai.getAwalKerja();
            Date date2 = pegawai.getAkhirKerja();
            DateTime dateTime1 = new DateTime(date1);
            DateTime dateTime2 = new DateTime(date2);

            Period period = new Period(dateTime1, dateTime2);

            PeriodFormatter formatter = new PeriodFormatterBuilder()
                    .appendYears().appendSuffix(" tahun ")
                    .appendMonths().appendSuffix(" bulan ")
                    .appendWeeks().appendSuffix(" minggu ")
                    .appendDays().appendSuffix(" hari ")
                    .printZeroNever()
                    .toFormatter();

            String elapsed = formatter.print(period);

            pegawai.setMasaKerja(elapsed);
            String usia = hitungUsia(pegawai.getTglLahir());
            pegawai.setUsia(usia);

            log.info("Hasilnya : " + elapsed);
            log.info("Usia : " + usia);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return pegawaiRepo.saveAndFlush(pegawai);
    }

    public List<String> search(String keyword) {
        return pegawaiRepo.search(keyword);
    }


}
