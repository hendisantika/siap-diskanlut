package com.dewipurnamasari.diskanlut.siap.service;

import com.dewipurnamasari.diskanlut.siap.entity.SK;
import com.dewipurnamasari.diskanlut.siap.repository.SKRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/07/18
 * Time: 17.27
 * To change this template use File | Settings | File Templates.
 */

@Service
@Transactional
public class SKService {
    @Autowired
    SKRepo skRepo;

    public List<SK> findAll() {
        return skRepo.findAll();
    }

    public SK findOne(String id) {
        return skRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "SK " + id + " Not found"));
    }

    public SK save(SK sk) {
        return skRepo.saveAndFlush(sk);
    }

    public void delete(String id) {
        skRepo.deleteById(id);
    }

}
