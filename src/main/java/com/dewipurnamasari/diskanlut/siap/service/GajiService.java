package com.dewipurnamasari.diskanlut.siap.service;

import com.dewipurnamasari.diskanlut.siap.dto.GajiForm;
import com.dewipurnamasari.diskanlut.siap.dto.RealisasiDTO;
import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import com.dewipurnamasari.diskanlut.siap.entity.Gaji;
import com.dewipurnamasari.diskanlut.siap.repository.GajiRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static com.dewipurnamasari.diskanlut.siap.utils.DiskanlutUtils.convertStringToDecimal;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/07/18
 * Time: 19.05
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
public class GajiService {

    @Autowired
    GajiRepo gajiRepo;

    public List<Gaji> findAll() {
        return (List<Gaji>) gajiRepo.findAll();
    }

    public Gaji findOne(String id) {
        return gajiRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Gaji " + id + " Not found"));
    }

    public Gaji save(GajiForm gajiForm) {
        Gaji gaji = new Gaji();
        gaji.setGajiId(gajiForm.getGajiId());
        gaji.setNama(gajiForm.getNama());
        gaji.setBulan(gajiForm.getBulan());
        gaji.setNoRekening(gajiForm.getNoRekening());
        gaji.setNamaBank(gajiForm.getNamaBank());
        gaji.setTerbilang(gajiForm.getTerbilang());
        gaji.setTempat(gajiForm.getTempat());
        gaji.setJumlah(convertStringToDecimal(gajiForm.getJumlah()));
        gaji.setBpjsTk(convertStringToDecimal(gajiForm.getBpjsTk()));
        gaji.setBpjsKs(convertStringToDecimal(gajiForm.getBpjsKs()));
        gaji.setPph(convertStringToDecimal(gajiForm.getPph()));
        gaji.setTotal(convertStringToDecimal(gajiForm.getTotal()));
        gaji.setYgMenerima(gajiForm.getYgMenerima());
        gaji.setBidang(gajiForm.getBidang());
        gaji.setKpa(gajiForm.getKpa());
        gaji.setBendahara(gajiForm.getBendahara());

        return gajiRepo.save(gaji);
    }

    public void delete(String id) {
        gajiRepo.deleteById(id);
    }

    public List<RealisasiDTO> getRealisasiByBidang(int bidangId) {
        return gajiRepo.getRealisasiByBidang(bidangId);
    }

    public List<Gaji> getRealisasi() {
        return gajiRepo.getRealisasi();
    }

    public List<Gaji> findByBulanAndBidang(int bulan, int bidangId) {
        Bidang bidang = new Bidang();
        bidang.setId((long) bidangId);
        return gajiRepo.findByBulanAndBidangIs(bulan, bidang);
    }


}
