package com.dewipurnamasari.diskanlut.siap.service;

import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import com.dewipurnamasari.diskanlut.siap.entity.Keuangan;
import com.dewipurnamasari.diskanlut.siap.repository.KeuanganRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/09/18
 * Time: 20.02
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
@Slf4j
public class KeuanganService {
    @Autowired
    KeuanganRepo keuanganRepo;

    public List<Keuangan> findAll() {
        return keuanganRepo.findAll();
    }

    public Keuangan findOne(String id) {
        return keuanganRepo.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Keuangan " + id + " Not found"));
    }

    public Keuangan save(Keuangan keuangan) {
        return keuanganRepo.saveAndFlush(keuangan);
    }

    public void delete(Keuangan keuangan) {
        keuanganRepo.delete(keuangan);
    }

    public Keuangan findById(Bidang bidangId) {
        log.info("Bidang ID {}", bidangId);
        log.info("Data Laporan Keuangan {}", bidangId);
        return keuanganRepo.findByBidangIs(bidangId);
    }

}
