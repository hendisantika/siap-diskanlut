package com.dewipurnamasari.diskanlut.siap.dto;

import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import com.dewipurnamasari.diskanlut.siap.entity.Jabatan;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 28/07/18
 * Time: 19.42
 * To change this template use File | Settings | File Templates.
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class GajiForm implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String gajiId;

    private String nama;

    private Long noRekening;

    private String namaBank;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bidang_id", referencedColumnName = "id")
    private Bidang bidang;

    private int bulan;

    private String jumlah;

    private String bpjsTk;

    private String bpjsKs;

    private String pph;

    private String total;

    private String terbilang;

    private String tempat;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "kpa_id")
    private Jabatan kpa;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bendahara_id")
    private Jabatan bendahara;

    private String ygMenerima;
}
