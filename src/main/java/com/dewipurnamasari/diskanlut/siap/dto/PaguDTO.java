package com.dewipurnamasari.diskanlut.siap.dto;

import com.dewipurnamasari.diskanlut.siap.entity.Bidang;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/09/18
 * Time: 07.40
 * To change this template use File | Settings | File Templates.
 */
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PaguDTO {
    @Id
    private String id;

    @NotNull
    @Column(unique = true)
    private String dpaId;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "bidang_id", referencedColumnName = "id")
    private Bidang bidang;

    private String kdProgram;

    private String kdKegiatan;

    private String kdRekening;

    private String jmlAnggaran;

    private Long jmlOrang;

    private int jmlBulan;
}
