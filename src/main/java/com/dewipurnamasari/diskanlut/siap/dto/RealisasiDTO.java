package com.dewipurnamasari.diskanlut.siap.dto;

import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/10/18
 * Time: 08.19
 * To change this template use File | Settings | File Templates.
 */

public interface RealisasiDTO {
//    Long getId();

    Long getBidangId();

    String getBulan();

    BigDecimal getTotal();

}
