package com.dewipurnamasari.diskanlut.siap.dbloader;

import com.dewipurnamasari.diskanlut.siap.entity.Address;
import com.dewipurnamasari.diskanlut.siap.entity.User;
import com.dewipurnamasari.diskanlut.siap.entity.UserRole;
import com.dewipurnamasari.diskanlut.siap.repository.UserRepository;
import com.dewipurnamasari.diskanlut.siap.repository.UserRoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 24/07/18
 * Time: 16.06
 * To change this template use File | Settings | File Templates.
 */
//@Component
@Slf4j
public class UserDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        User admin = new User();
        admin.setUsername("admin");
        admin.setPassword("123456");
        admin.setFirstName("Dewi");
        admin.setLastName("Purnamasari");

        Set<Address> addressSet = new HashSet<Address>(0);
        Address adminAddr1 = new Address();
        adminAddr1.setAddress1("Street Address 1-1");
        adminAddr1.setAddress2("Street Address 1-2");
        adminAddr1.setAddrCity("Los Angeles");
        adminAddr1.setAddrState("CA");
        adminAddr1.setZipCode("90001");
        adminAddr1.setUser(admin);
        addressSet.add(adminAddr1);

        Address adminAddr2 = new Address();
        adminAddr2.setAddress1("Street Address 2-1");
        adminAddr2.setAddress2("Street Address 2-1");
        adminAddr2.setAddrCity("Los Angeles");
        adminAddr2.setAddrState("CA");
        adminAddr2.setZipCode("90002");
        adminAddr2.setUser(admin);
        addressSet.add(adminAddr2);

        UserRole adminRole = new UserRole();
        adminRole.setUserRoleName("Administrator");
        adminRole.setUser(admin);
        Set<UserRole> adminRoles = new HashSet<UserRole>(0);
        adminRoles.add(adminRole);

        admin.setAddressSet(addressSet);
        admin.setUserRoles(adminRoles);

        userRepository.save(admin);
        log.info("Saved admin ID: " + admin.getUserId());


        User user1 = new User();
        user1.setUsername("hendisantika");
        user1.setPassword("123456");
        user1.setFirstName("Hendi");
        user1.setLastName("Santika");

        UserRole userRole = new UserRole();
        userRole.setUserRoleName("User");
        userRole.setUser(user1);
        Set<UserRole> userRoles = new HashSet<UserRole>(0);
        userRoles.add(userRole);

        user1.setUserRoles(userRoles);

        userRepository.save(user1);
        log.info("Saved user1 ID: " + user1.getUserId());

    }

}

