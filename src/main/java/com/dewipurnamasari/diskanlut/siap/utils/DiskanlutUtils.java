package com.dewipurnamasari.diskanlut.siap.utils;

import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA.
 * Project : siap-diskanlut
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/09/18
 * Time: 07.44
 * To change this template use File | Settings | File Templates.
 */
public class DiskanlutUtils {
    public static BigDecimal convertToNumber(String rupiah) {
        String hasil = rupiah.replaceAll("\\D", "");
        return new BigDecimal(hasil);
    }

    public static Double convertStringToDecimal(String rupiah) {
        String hasil = rupiah.replaceAll("\\D", "");
        return Double.valueOf(hasil);
    }
}
