$('.print').on('click', function () {
    // print();
    var cloning = $(".container-fluid").clone();
    cloning.find("input, table, select, textarea, h2").each(function () {
        if ($(this).is("[type='checkbox']") || $(this).is("[type='checkbox']")) {
            $(this).css({'border': 0, 'margin-bottom': '20px'}).attr("checked", $(this).attr("checked"));
        } else if ($(this).is("table.bordered")) {
            $(this).attr("border", 1);
        }
        else if ($(this).is("h2")) {
            $(this).css({"text-align": "center", "font-size": "30px"});
        }
        else {
            $(this).css({'border': 0, 'margin-bottom': '20px'}).attr("value", !!$(this).val() ? $(this).val() : '-');
        }
    });

    var content = cloning.html();
    var newWdw = window.open('', 'blank');
    newWdw.document.body.innerHTML = content;
    setTimeout(function () {
        newWdw.print();
        newWdw.close();
    }, 0)
});
