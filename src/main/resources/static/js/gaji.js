$(document).ready(function () {
    var table = $('#gaji').dataTable({
        "sAjaxSource": "/api/gaji",
        "sAjaxDataProp": "",
        "order": [[0, "asc"]],
        "columns": [
            {"data": "gajiId"},
            {"data": "nama"},
            {"data": "noRekening"},
            {"data": "namaBank"},
            {"data": "bidang.namaBidang"},
            {"data": "bulan"},
            {"data": "jumlah"},
            {"data": "bpjsTk"},
            {"data": "bpjsKs"},
            {"data": "pph"},
            {"data": "total"},
            {
                "data": "id",
                "render": function (data, type, full, meta) {
                    return '<a class="btn btn-info btn-sm" href="/gaji/view/' + full.gajiId + '">View</a>';
                }
            },
            {
                "data": "id",
                "render": function (data, type, full, meta) {
                    return '<a class="btn btn-warning btn-sm" href="/gaji/edit/' + full.gajiId + '">Edit</a>';
                }
            },
            {
                "data": "id",
                "render": function (data, type, full, meta) {
                    return '<a class="btn btn-danger btn-sm" onclick="javascript:return confirm(\'Anda yakin menghapus product ini ?\');" href="/gaji/delete/' + full.gajiId + '">Delete</a>';
                }
            }
        ]
    });
});
