$('#idBidang').on('change', getAllRequiredData);

function getAllRequiredData() {
    var x = document.documentURI;
    var ori = window.location.origin;
    var idBidang = document.getElementById("idBidang").value;
    var url1 = ori + "/api/pagu/" + idBidang;
    var url2 = ori + "/api/laporan/keuangan/" + idBidang;
    var url3 = ori + "/api/gaji/realisasi/" + idBidang;
    var url4 = ori + "/api/laporan/fisikId/" + idBidang;


    $.when(
        $.ajax({
            url: url1,
            method: "GET",
            dataType: "json",
            success: function (response) {
                $('#dpaId').val(response.dpaId);
                $('#kdProgram').val(response.kdProgram);
                $('#kdKegiatan').val(response.kdKegiatan);
                $('#kdRekening').val(response.kdRekening);
                $('#jmlAnggaran').val(response.jmlAnggaran);
                $('#jmlOrang').val(response.jmlOrang);
                $('#jmlBulan').val(response.jmlBulan);
                console.log(response.dpaId)
            }
        }),
        $.ajax({
            url: url2,
            method: "GET",
            dataType: "json",
            success: function (response) {
                for (var i = 1; i <= 12; i++) {
                    var t1 = !!(response['t' + i]) ? ((+($("#t" + i).val()) + response['t' + i]) / $('#jmlAnggaran').val() * 100).toFixed(2) : '';
                    var r1 = !!(response['r' + i]) ? ((+($("#r" + i).val()) + response['r' + i]) / $('#jmlAnggaran').val() * 100).toFixed(2) : '';
                    $('#t' + i).val(t1);
                    $('#r' + i).val(r1);
                }
            }
        }).fail(function () {
            $('#lapId').val('');
            $('#t1').val('');
            $('#t2').val('');
            $('#t3').val('');
            $('#t4').val('');
            $('#t5').val('');
            $('#t6').val('');
            $('#t7').val('');
            $('#t8').val('');
            $('#t9').val('');
            $('#t10').val('');
            $('#t11').val('');
            $('#t12').val('');
        }),
        $.ajax({
            url: url4,
            method: "GET",
            dataType: "json",
            success: function (response) {
                $('#lapId').val(!!response.id ? response.id : '');
            }
        }).fail(function () {
            $('#lapId').val('');
        })
    ).then(function () {
        console.log('all complete');
    });
}


