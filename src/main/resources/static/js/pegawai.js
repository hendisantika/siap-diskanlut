$(document).ready(function () {
    var table = $('#pegawai').dataTable({
        "sAjaxSource": "/api/pegawai",
        "sAjaxDataProp": "",
        "order": [[0, "asc"]],
        "columns": [
            {"data": "pegawaiId"},
            {"data": "nama"},
            {"data": "bidang.namaBidang"},
            {"data": "tglLahir"},
            {"data": "usia"},
            {"data": "sex"},
            {"data": "pendidikan"},
            {"data": "namaSekolah"},
            {"data": "jurusan"},
            {"data": "tahunLulus"},
            {"data": "jabatan"},
            {"data": "masaKerja"},
            {
                "data": "id",
                "render": function (data, type, full, meta) {
                    return '<a class="btn btn-info btn-sm" href="/pegawai/' + data + '?aksi=view">View</a>';
                }
            },
            {
                "data": "id",
                "render": function (data, type, full, meta) {
                    return '<a class="btn btn-warning btn-sm" href="/pegawai/' + data + '?aksi=edit">Edit</a>';
                }
            },
            {
                "data": "id",
                "render": function (data, type, full, meta) {
                    return '<a class="btn btn-danger btn-sm" onclick="javascript:return confirm(\'Anda yakin menghapus product ini ?\');" href="/pegawai/delete/' + data + '">Delete</a>';
                }
            }
        ]
    });
});
