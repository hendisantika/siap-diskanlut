// $('#lapBidang').on('change', getLaporanKeuangan, test);
$('#idBidang').on('change', getAllRequiredData);

function getDataPagu() {
    var x = document.documentURI;
    var ori = window.location.origin;
    var idBidang = document.getElementById("idBidang").value;

    // var url = encodeURI("http://localhost:8080/api/pagu/" + idBidang);
    var url = ori + "/api/pagu/" + idBidang;
    console.log(url);
    $.ajax({
        url: url,
        method: "GET",
        dataType: "json",
        success: function (response) {
            if ($.isEmptyObject(response)) {
                alert("Data Bidang not found!")
            } else {
                $('#dpaId').val(response.dpaId);
                $('#kdProgram').val(response.kdProgram);
                $('#kdKegiatan').val(response.kdKegiatan);
                $('#kdRekening').val(response.kdRekening);
                $('#jmlAnggaran').val(response.jmlAnggaran);
                $('#jmlOrang').val(response.jmlOrang);
                $('#jmlBulan').val(response.jmlBulan);
            }
        }
    });
}


function getLaporanKeuangan() {
    var x = document.documentURI;
    var ori = window.location.origin;
    var idBidang = document.getElementById("lapBidang").value;

    // var url = encodeURI("http://localhost:8080/api/pagu/" + idBidang);
    var url = ori + "/api/laporan/keuangan/" + idBidang;
    console.log(url);
    $.ajax({
        url: url,
        method: "GET",
        dataType: "json",
        success: function (response) {
            if (response == null) {
                alert("Data Bidang not found!")
            } else {
                $('#t1').val(response.t1);
                $('#t2').val(response.t2);
                $('#t3').val(response.t3);
                $('#t4').val(response.t4);
                $('#t5').val(response.t5);
                $('#t6').val(response.t6);
                $('#t7').val(response.t7);
                $('#t8').val(response.t8);
                $('#t9').val(response.t9);
                $('#t10').val(response.t10);
                $('#t11').val(response.t11);
                $('#t12').val(response.t12);
                $('#r1').val(response.r1);
                $('#r2').val(response.r2);
                $('#r3').val(response.r3);
                $('#r4').val(response.r4);
                $('#r5').val(response.r5);
                $('#r6').val(response.r6);
                $('#r7').val(response.r7);
                $('#r8').val(response.r8);
                $('#r9').val(response.r9);
                $('#r10').val(response.r10);
                $('#r11').val(response.r11);
                $('#r12').val(response.r12);
            }
        }
    });
}

function getAllRequiredData() {
    var x = document.documentURI;
    var ori = window.location.origin;
    var idBidang = document.getElementById("idBidang").value;
    var url1 = ori + "/api/pagu/" + idBidang;
    var url2 = ori + "/api/laporan/keuangan/" + idBidang;
    var url3 = ori + "/api/gaji/realisasi/" + idBidang;


    $.when(
        $.ajax({
            url: url1,
            method: "GET",
            dataType: "json",
            success: function (response) {
                $('#dpaId').val(response.dpaId);
                $('#kdProgram').val(response.kdProgram);
                $('#kdKegiatan').val(response.kdKegiatan);
                $('#kdRekening').val(response.kdRekening);
                $('#jmlAnggaran').val(response.jmlAnggaran);
                $('#jmlOrang').val(response.jmlOrang);
                $('#jmlBulan').val(response.jmlBulan);
                console.log(response.dpaId)
            }
        }),
        $.ajax({
            url: url2,
            method: "GET",
            dataType: "json",
            success: function (response) {
                $('#lapId').val(!!response.id ? response.id : '');
                $('#t1').val(!!response.t1 ? response.t1 : '');
                $('#t2').val(!!response.t2 ? response.t2 : '');
                $('#t3').val(!!response.t3 ? response.t3 : '');
                $('#t4').val(!!response.t4 ? response.t4 : '');
                $('#t5').val(!!response.t5 ? response.t5 : '');
                $('#t6').val(!!response.t6 ? response.t6 : '');
                $('#t7').val(!!response.t7 ? response.t7 : '');
                $('#t8').val(!!response.t8 ? response.t8 : '');
                $('#t9').val(!!response.t9 ? response.t9 : '');
                $('#t10').val(!!response.t10 ? response.t10 : '');
                $('#t11').val(!!response.t11 ? response.t11 : '');
                $('#t12').val(!!response.t12 ? response.t12 : '');
                console.log("lapId : " + response.id)
            }
        }).fail(function () {
            $('#lapId').val('');
            $('#t1').val('');
            $('#t2').val('');
            $('#t3').val('');
            $('#t4').val('');
            $('#t5').val('');
            $('#t6').val('');
            $('#t7').val('');
            $('#t8').val('');
            $('#t9').val('');
            $('#t10').val('');
            $('#t11').val('');
            $('#t12').val('');
        }),
        $.ajax({
            url: url3,
            method: "GET",
            dataType: "json",
            success: function (response) {
                $('#r1').val(response[0] && response[0].total ? response[0].total : '');
                for (var i = 2; i <= 12; i++) {
                    var index = i - 1;
                    $("#r" + i).val(!!(response[index]) ? +($("#r" + (index)).val()) + +(response[index].total) : '');
                }
            }
        })
    ).then(function () {
        console.log('all complete');
    });
}


