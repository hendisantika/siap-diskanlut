$(document).ready(function () {
    $('#nama').autocomplete({
        source: 'http://localhost:8080/api/pegawai/search'
    });
});

var ori = window.location.origin;
var options = {
    // url: 'http://localhost:8080/api/pegawai',
    url: ori + '/api/pegawai',
    getValue: "nama",
    list: {
        match: {
            enabled: true
        },
        onClickEvent: function () {
            document.getElementById("ygMenerima").value = nama.value;
        }
    }
};
$("#nama").easyAutocomplete(options);