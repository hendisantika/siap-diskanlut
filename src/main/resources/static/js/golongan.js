var pangkatObject = {
    "I-A": "Juru Muda",
    "I-B": "Juru Muda Tingkat I",
    "I-C": "Juru",
    "I-D": "Juru Tingkat I",
    "II-A": "Pengatur Muda",
    "II-B": "Pengatur Muda Tk I",
    "II-C": "Pengatur",
    "II-D": "Pengatur Tingkat I",
    "III-A": "Penata Muda",
    "III-B": "Penata Muda Muda Tk I",
    "III-C": "Penata",
    "III-D": "Penata Tingkat I",
    "IV-A": "Pembina Muda",
    "IV-B": "Pembina Muda Muda Tk I",
    "IV-C": "Pembina",
    "IV-D": "Pembina Tingkat I",
    "IV-E": "Pembina Utama",

};

$(document).on('load', function () {
    var golongan = $('#golongan').val();
    updatePangkat(golongan);
});

$("#golongan").on("change", function () {
    var golongan = $(this).val();
    updatePangkat(golongan)
});

function updatePangkat(value) {
    $("#pangkat").val(pangkatObject[value]);
}