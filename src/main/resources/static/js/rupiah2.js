var jumlah = document.getElementById('jumlah');
var bpjsTk = document.getElementById('bpjsTk');
var bpjsKs = document.getElementById('bpjsKs');
var pph = document.getElementById('pph');
var nama = document.getElementById('nama');

var ygMenerima = document.getElementById('ygMenerima').value;
jumlah.addEventListener('keyup', function (e) {
    jumlah.value = formatRupiah(this.value, 'Rp. ');
    hitungGaji();
});
bpjsTk.addEventListener('keyup', function (e) {
    bpjsTk.value = formatRupiah(this.value, 'Rp. ');
    hitungGaji();
});
bpjsKs.addEventListener('keyup', function (e) {
    bpjsKs.value = formatRupiah(this.value, 'Rp. ');
    hitungGaji();
});
pph.addEventListener('keyup', function (e) {
    pph.value = formatRupiah(this.value, 'Rp. ');
    hitungGaji();
});
nama.addEventListener('keyup', function (e) {
    document.getElementById("ygMenerima").value = nama.value;
});

/* Fungsi Format Rupiah */
function formatRupiah(angka, prefix) {
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split = number_string.split(','),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function convertToAngka(rupiah) {

    return parseInt(rupiah.replace(/,.*|[^0-9]/g, ''), 10);

}


function hitungGaji() {
    var potongan = ['#bpjsTk', '#bpjsKs', '#pph'];
    var pendapatan = convertToAngka(jumlah.value);

    for (var i = 0; i < potongan.length; i++) {
        pendapatan -= convertToAngka($(potongan[i]).val());
        $('#total').val(formatRupiah(pendapatan.toString(), 'Rp. '));
    }

    var feed = convertToAngka($('#total').val()).toString();
    if (isNaN(feed)) {
        return false;
    }
    $('#currency').html(currency(feed));
    $('#terbilang').html(digit3(feed));
}