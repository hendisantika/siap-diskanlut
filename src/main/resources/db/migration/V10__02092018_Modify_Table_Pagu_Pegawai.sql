-- Modify Tabel Pagu

ALTER TABLE pagu ADD kd_program VARCHAR (255);

ALTER TABLE pagu ADD kd_kegiatan VARCHAR (255);

ALTER TABLE pagu ADD kd_rekening VARCHAR (255);

-- Modify Tabel Gaji

ALTER TABLE gaji ADD no_rekening BIGINT;

ALTER TABLE gaji ADD nama_bank VARCHAR(255);

-- Modify Tabel Pegawai

ALTER TABLE pegawai ADD no_telpon VARCHAR(20);

ALTER TABLE pegawai ADD status_merital VARCHAR(20);

ALTER TABLE pegawai ADD jml_anak INT(5);

ALTER TABLE pegawai ADD alamat VARCHAR (255);
