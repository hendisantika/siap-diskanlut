INSERT INTO tbl_user (id, first_name, last_name, password, username)
VALUES ('554a8eb5-b02a-45e9-8d39-f28cd52d4be5', 'Hendi', 'Santika',
        '$2a$04$pebJp6qoTsWOVOB.rdMwT.CCiQuA4XeA6sF3FcgtSeeseXm/Y7CkK', 'hendisantika'); -- -- 123456
INSERT INTO tbl_user (id, first_name, last_name, password, username)
VALUES ('cacecaff-dd85-4485-8aed-4f823b1850fa', 'Dewi', 'Purnamasari',
        '$2a$04$pebJp6qoTsWOVOB.rdMwT.CCiQuA4XeA6sF3FcgtSeeseXm/Y7CkK', 'admin'); -- -- 123456


INSERT INTO tbl_user_role (id, role_name, user_id)
VALUES ('32c321f8-e7b9-49bb-9656-9e7e82b2d4dc', 'Administrator', 'cacecaff-dd85-4485-8aed-4f823b1850fa');
INSERT INTO tbl_user_role (id, role_name, user_id)
VALUES ('c52ae4e0-8c8b-4eff-a77f-c02498fd4bfa', 'User', '554a8eb5-b02a-45e9-8d39-f28cd52d4be5');

INSERT INTO tbl_address (id, city, state, address1, address2, zip_code, user_id)
VALUES (1, 'Los Angeles', 'CA', 'Street Address 2-1', 'Street Address 2-1', '90002',
        'cacecaff-dd85-4485-8aed-4f823b1850fa');
INSERT INTO tbl_address (id, city, state, address1, address2, zip_code, user_id)
VALUES (2, 'Los Angeles', 'CA', 'Street Address 1-1', 'Street Address 1-2', '90001',
        'cacecaff-dd85-4485-8aed-4f823b1850fa');

INSERT INTO bidang (id, nama_bidang)
VALUES (1, 'Kelautan');
INSERT INTO bidang (id, nama_bidang)
VALUES (2, 'Sekretariat');
INSERT INTO bidang (id, nama_bidang) VALUES (3, 'Bina Usaha');
INSERT INTO bidang (id, nama_bidang) VALUES (4, 'Budidaya');
INSERT INTO bidang (id, nama_bidang) VALUES (5, 'Perikanan Tangkap');