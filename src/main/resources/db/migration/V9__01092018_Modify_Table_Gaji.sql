ALTER TABLE gaji DROP COLUMN jml_hari;

ALTER TABLE gaji DROP COLUMN bidang;

ALTER TABLE gaji DROP COLUMN kpa;

ALTER TABLE gaji DROP COLUMN bendahara;

ALTER TABLE gaji ADD bidang_id BIGINT(20);

ALTER TABLE gaji ADD kpa_id VARCHAR(255);

ALTER TABLE gaji ADD bendahara_id VARCHAR(255);

ALTER TABLE gaji ADD CONSTRAINT FK_Gaji_Bidang FOREIGN KEY (bidang_id) REFERENCES bidang (id);

ALTER TABLE gaji ADD CONSTRAINT FK_Gaji_Jabatan_KPA FOREIGN KEY (kpa_id) REFERENCES jabatan (jabatan_id);

ALTER TABLE gaji ADD CONSTRAINT FK_Gaji_Jabatan_BDHR FOREIGN KEY (bendahara_id) REFERENCES jabatan (jabatan_id);


