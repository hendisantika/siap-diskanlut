-- Tabel Pagu
ALTER TABLE pagu
  ADD bidang_id BIGINT(20);

ALTER TABLE pagu ENGINE = InnoDB;
ALTER TABLE pagu
  ADD CONSTRAINT FK_Pagu_Bidang
FOREIGN KEY (bidang_id) REFERENCES bidang (id);

-- Tabel Pegawai
ALTER TABLE pegawai
  ADD bidang_id BIGINT(20);

ALTER TABLE pegawai ENGINE = InnoDB;
ALTER TABLE pegawai
  ADD CONSTRAINT FK_Pegawai_Bidang
FOREIGN KEY (bidang_id) REFERENCES bidang (id);

-- Tabel SK
ALTER TABLE sk
  ADD bidang_id BIGINT(20);

ALTER TABLE sk ENGINE = InnoDB;
ALTER TABLE sk
  ADD CONSTRAINT FK_SK_Bidang
FOREIGN KEY (bidang_id) REFERENCES bidang (id);
