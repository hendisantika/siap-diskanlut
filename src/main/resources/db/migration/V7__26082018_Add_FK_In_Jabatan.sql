-- ALTER TABLE jabatan DROP COLUMN id;
ALTER TABLE jabatan
  ADD bidang_id BIGINT(20);

ALTER TABLE jabatan
  ENGINE = InnoDB;
ALTER TABLE jabatan
  ADD CONSTRAINT FK_Jabatan_Bidang
FOREIGN KEY (bidang_id) REFERENCES bidang (id);
