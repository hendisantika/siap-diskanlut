CREATE TABLE laporan_keuangan (
  id VARCHAR (255) NOT NULL PRIMARY KEY,
  bidang_id BIGINT (20) NOT NULL,
  t1 DECIMAL (12,2),
  t2 DECIMAL (12,2),
  t3 DECIMAL (12,2),
  t4 DECIMAL (12,2),
  t5 DECIMAL (12,2),
  t6 DECIMAL (12,2),
  t7 DECIMAL (12,2),
  t8 DECIMAL (12,2),
  t9 DECIMAL (12,2),
  t10 DECIMAL (12,2),
  t11 DECIMAL (12,2),
  t12 DECIMAL (12,2),
  r1 DECIMAL (12,2),
  r2 DECIMAL (12,2),
  r3 DECIMAL (12,2),
  r4 DECIMAL (12,2),
  r5 DECIMAL (12,2),
  r6 DECIMAL (12,2),
  r7 DECIMAL (12,2),
  r8 DECIMAL (12,2),
  r9 DECIMAL (12,2),
  r10 DECIMAL (12,2),
  r11 DECIMAL (12,2),
  r12 DECIMAL (12,2),
  created DATETIME NOT NULL,
  modified DATETIME,
  CONSTRAINT FK_Keuangan_Bidang
  FOREIGN KEY (bidang_id)
  REFERENCES bidang (id)
  ON UPDATE CASCADE
  ON DELETE CASCADE
);