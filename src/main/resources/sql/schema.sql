CREATE TABLE tbl_user (
	id VARCHAR(36) PRIMARY KEY
	,first_name VARCHAR(128) DEFAULT 'NULL' NULL
	,last_name VARCHAR(128) DEFAULT 'NULL' NULL
	,password VARCHAR(256) DEFAULT 'NULL' NULL
	,username VARCHAR(64) DEFAULT 'NULL' NULL
	,CONSTRAINT UK_k0bty7tbcye41jpxam88q5kj2 UNIQUE (username)
	);

CREATE TABLE tbl_user_role (
	id VARCHAR(36) PRIMARY KEY
	,role_name VARCHAR(128) NOT NULL
	,user_id VARCHAR(36) DEFAULT 'NULL' NULL
	,CONSTRAINT UK_k0ijfnxd35ym2mj3fniha0spw UNIQUE (role_name)
	,CONSTRAINT FKggc6wjqokl2vlw89y22a1j2oh FOREIGN KEY (user_id) REFERENCES tbl_user(id)
	);

CREATE TABLE tbl_address (
	id BIGINT auto_increment PRIMARY KEY
	,city VARCHAR(256) DEFAULT 'NULL' NULL
	,STATE VARCHAR(64) DEFAULT 'NULL' NULL
	,address1 VARCHAR(256) DEFAULT 'NULL' NULL
	,address2 VARCHAR(256) DEFAULT 'NULL' NULL
	,zip_code VARCHAR(32) DEFAULT 'NULL' NULL
	,user_id VARCHAR(36) DEFAULT 'NULL' NULL
	,CONSTRAINT FKlo13i087wmqhi0h7ffjxoljrb FOREIGN KEY (user_id) REFERENCES tbl_user(id)
	);

CREATE TABLE bidang (
	id BIGINT NOT NULL auto_increment
	,nama_bidang VARCHAR(255)
	,PRIMARY KEY (id)
	);

CREATE TABLE gaji (
	gaji_id VARCHAR(255) NOT NULL
	,bendahara VARCHAR(255)
	,bidang VARCHAR(255)
	,bpjs_ks FLOAT
	,bpjs_tk FLOAT
	,bulan VARCHAR(255)
	,created_date DATETIME NOT NULL
	,jml_hari INT NOT NULL
	,jumlah FLOAT
	,kpa VARCHAR(255)
	,nama VARCHAR(255)
	,pph FLOAT
	,tempat VARCHAR(255)
	,terbilang VARCHAR(255)
	,total FLOAT
	,yg_menerima VARCHAR(255)
	,PRIMARY KEY (gaji_id)
	);

CREATE TABLE jabatan (
	jabatan_id VARCHAR(255) NOT NULL
	,created_date DATETIME
	,fungsional VARCHAR(255)
	,golongan VARCHAR(255)
	,nama VARCHAR(255)
	,nip VARCHAR(255)
	,pangkat VARCHAR(255)
	,struktural VARCHAR(255)
	,PRIMARY KEY (jabatan_id)
	);

CREATE TABLE pagu (
	id VARCHAR(255) NOT NULL
	,dpa_id VARCHAR(255) NOT NULL
	,jml_anggaran FLOAT
	,jml_bulan INT NOT NULL
	,jml_orang BIGINT
	,PRIMARY KEY (id)
	);

CREATE TABLE pegawai (
	id VARCHAR(255) NOT NULL
	,agama VARCHAR(255)
	,akhir_kerja DATE
	,awal_kerja DATE
	,jabatan VARCHAR(255)
	,jurusan VARCHAR(255)
	,nama VARCHAR(255)
	,nama_sekolah VARCHAR(255)
	,pegawai_id BIGINT
	,pendidikan VARCHAR(255)
	,sex VARCHAR(255)
	,tahun_lulus INT NOT NULL
	,tempat_lahir VARCHAR(255)
	,tgl_lahir DATE
	,PRIMARY KEY (id)
	);

CREATE TABLE sk (
	sk_id VARCHAR(255) NOT NULL
	,created_date DATETIME NOT NULL
	,nosk VARCHAR(255)
	,tentang VARCHAR(255)
	,tglsk DATETIME
	,PRIMARY KEY (sk_id)
	);