create table gaji
(
  gaji_id      bigint auto_increment
    primary key,
  bendahara    varchar(255) default 'NULL' null,
  bidang       varchar(255) default 'NULL' null,
  bpjs_ks      double default 'NULL'       null,
  bpjs_tk      double default 'NULL'       null,
  bulan        varchar(255) default 'NULL' null,
  created_date datetime                    not null,
  jml_hari     int                         not null,
  jumlah       double default 'NULL'       null,
  kpa          varchar(255) default 'NULL' null,
  nama         varchar(255) default 'NULL' null,
  pph          double default 'NULL'       null,
  tempat       varchar(255) default 'NULL' null,
  terbilang    varchar(255) default 'NULL' null,
  total        double default 'NULL'       null,
  yg_menerima  varchar(255) default 'NULL' null
);

create table struktural
(
  pa_id      bigint auto_increment
    primary key,
  fungsional varchar(255) default 'NULL' null,
  golongan   varchar(255) default 'NULL' null,
  struktural varchar(255) default 'NULL' null,
  nama       varchar(255) default 'NULL' null,
  nip        varchar(255) default 'NULL' null,
  pangkat    varchar(255) default 'NULL' null
);

create table pagu
(
  dpa_id       bigint auto_increment
    primary key,
  jml_anggaran double default 'NULL' null,
  jml_bulan    int                   not null,
  jml_orang    bigint default 'NULL' null
);

create table pegawai
(
  pegawai_id   bigint auto_increment
    primary key,
  agama        varchar(255) default 'NULL' null,
  akhir_kerja  datetime default 'NULL'     null,
  awal_kerja   datetime default 'NULL'     null,
  struktural   varchar(255) default 'NULL' null,
  jurusan      varchar(255) default 'NULL' null,
  nama         varchar(255) default 'NULL' null,
  nama_sekolah varchar(255) default 'NULL' null,
  pendidikan   varchar(255) default 'NULL' null,
  sex          varchar(255) default 'NULL' null,
  tahun_lulus  int                         not null,
  tempat_lahir varchar(255) default 'NULL' null,
  tgl_lahir    date default 'NULL'         null
);

create table sk
(
  sk_id        bigint auto_increment
    primary key,
  created_date datetime                    not null,
  nosk         varchar(255) default 'NULL' null,
  tentang      varchar(255) default 'NULL' null,
  tglsk        datetime default 'NULL'     null
);

create table tbl_user
(
  id         bigint auto_increment
    primary key,
  first_name varchar(128) default 'NULL' null,
  last_name  varchar(128) default 'NULL' null,
  password   varchar(256) default 'NULL' null,
  username   varchar(64) default 'NULL'  null,
  constraint UK_k0bty7tbcye41jpxam88q5kj2
  unique (username)
);

create table tbl_address
(
  id       bigint auto_increment
    primary key,
  city     varchar(256) default 'NULL' null,
  state    varchar(64) default 'NULL'  null,
  address1 varchar(256) default 'NULL' null,
  address2 varchar(256) default 'NULL' null,
  zip_code varchar(32) default 'NULL'  null,
  user_id  bigint default 'NULL'       null,
  constraint FKlo13i087wmqhi0h7ffjxoljrb
  foreign key (user_id) references tbl_user (id)
);

create table tbl_user_role
(
  id        int auto_increment
    primary key,
  role_name varchar(128)          not null,
  user_id   bigint default 'NULL' null,
  constraint UK_k0ijfnxd35ym2mj3fniha0spw
  unique (role_name),
  constraint FKggc6wjqokl2vlw89y22a1j2oh
  foreign key (user_id) references tbl_user (id)
);

INSERT INTO siap.pegawai (pegawai_id,
                          agama,
                          akhir_kerja,
                          awal_kerja, struktural,
                          jurusan,
                          nama,
                          nama_sekolah,
                          pendidikan,
                          sex,
                          tahun_lulus,
                          tempat_lahir,
                          tgl_lahir)
VALUES (1,
        'Islam',
        '2018-07-31 00:00:00',
        '2006-01-01 00:00:00',
        'THL',
        'KAT (Komputerisasi Akuntansi)',
        'Dewi Purnamasari',
        'Politeknik Piksi Ganesha',
        'd4',
        'Perempuan',
        2018,
        'Kota Cimahi',
        '1988-03-13');
INSERT INTO siap.pegawai (pegawai_id,
                          agama,
                          akhir_kerja,
                          awal_kerja, struktural,
                          jurusan,
                          nama,
                          nama_sekolah,
                          pendidikan,
                          sex,
                          tahun_lulus,
                          tempat_lahir,
                          tgl_lahir)
VALUES (2,
        'Islam',
        '2018-07-31 00:00:00',
        '2006-01-01 00:00:00',
        'THL',
        'Ekonomi',
        'Ria Setianingsih',
        'Universitas Winayamukti',
        's1',
        'Perempuan',
        2013,
        'Kota Bandung',
        '1980-04-21');
INSERT INTO siap.pegawai (pegawai_id,
                          agama,
                          akhir_kerja,
                          awal_kerja, struktural,
                          jurusan,
                          nama,
                          nama_sekolah,
                          pendidikan,
                          sex,
                          tahun_lulus,
                          tempat_lahir,
                          tgl_lahir)
VALUES (3,
        'Islam',
        '2018-07-31 00:00:00',
        '2016-01-01 00:00:00',
        'THL',
        'Pertanian',
        'Azalaea Sachi Krisan, S.Pi., M.P.',
        'Universitas Winayamukti',
        's2',
        'Perempuan',
        2016,
        'Kab. Bandung',
        '2018-05-12');
INSERT INTO siap.pegawai (pegawai_id,
                          agama,
                          akhir_kerja,
                          awal_kerja, struktural,
                          jurusan,
                          nama,
                          nama_sekolah,
                          pendidikan,
                          sex,
                          tahun_lulus,
                          tempat_lahir,
                          tgl_lahir)
VALUES (4,
        'Islam',
        '2018-07-31 00:00:00',
        '2011-01-01 00:00:00',
        'THL',
        'Kelautan',
        'Intan Wulansari, S.Kel',
        'Universitas Padjajaran',
        's1',
        'Perempuan',
        2010,
        'Kota Banjar',
        '1989-05-12');

INSERT INTO siap.sk (sk_id, created_date, nosk, tentang, tglsk)
VALUES (1,
        '2018-07-29 19:18:25',
        '814/Kep.22-Tangkap/2017',
        'Penunjukan Tenaga Pelaksana Harian Lepas pada Kegiatan Pendukung Peningkatan Sarana dan Prasarana Perikanan Tangkap Tahun Anggaran 2017',
        '2017-01-31 00:00:00');

INSERT INTO siap.tbl_address (id, city, state, address1, address2, zip_code, user_id)
VALUES (1, 'Los Angeles', 'CA', 'Street Address 1-1', 'Street Address 1-2', '90001', 1);
INSERT INTO siap.tbl_address (id, city, state, address1, address2, zip_code, user_id)
VALUES (2, 'Los Angeles', 'CA', 'Street Address 2-1', 'Street Address 2-1', '90002', 1);


INSERT INTO siap.tbl_user (id, first_name, last_name, password, username)
VALUES (1, 'Dewi', 'Purnamasari', '123456', 'admin');
INSERT INTO siap.tbl_user (id, first_name, last_name, password, username)
VALUES (2, 'Hendi', 'Santika', '123456', 'hendisantika');

INSERT INTO siap.tbl_user_role (id, role_name, user_id)
VALUES (1, 'Administrator', 1);
INSERT INTO siap.tbl_user_role (id, role_name, user_id)
VALUES (2, 'User', 2);
