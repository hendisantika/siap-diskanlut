-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2018 at 06:54 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siap`
--

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `pegawai_id` bigint(20) NOT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `akhir_kerja` datetime DEFAULT NULL,
  `awal_kerja` datetime DEFAULT NULL,
  `struktural` varchar(255) DEFAULT NULL,
  `jurusan` varchar(255) DEFAULT NULL,
  `nama_sekolah` varchar(255) DEFAULT NULL,
  `pendidikan` varchar(255) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `tahun_lulus` int(11) NOT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`pegawai_id`, `agama`, `akhir_kerja`, `awal_kerja`, `struktural`, `jurusan`, `nama_sekolah`, `pendidikan`, `sex`, `tahun_lulus`, `tempat_lahir`, `tgl_lahir`) VALUES
(1, 'Islam', '2017-12-31 00:00:00', '2006-01-01 00:00:00', 'Pengolah Data Pelabuhan', 'IPS', 'Yuppi Soreang', 'SMA', 'Perempuan', 2005, 'Cimahi', '1988-03-13'),
(2, 'Islam', '2006-01-01 00:00:00', '2017-12-31 00:00:00', 'Pengolah Statistik', 'Ekonomi', 'Universitas Winayamukti', 'S1', 'Perempuan', 2013, 'Bandung', '1981-04-21'),
(3, 'Islam', '2017-12-31 00:00:00', '2010-01-01 00:00:00', 'Petugas SAI', 'Agribisnis', 'Universitas Winayamukti', 'S2', 'Laki-Laki', 2015, 'Banten', '1985-09-12'),
(4, 'Islam', '2018-01-31 00:00:00', '2014-10-01 00:00:00', 'Petugas Kapal Perikanan', 'Agribisnis', 'Universitas Winayamukti', 'S2', 'Perempuan', 2015, 'Bandung', '1990-05-23'),
(5, 'Islam', '2017-12-31 00:00:00', '2013-01-01 00:00:00', 'Petugas SABMN', 'Perikanan', 'IPB', 'S1', 'Perempuan', 2012, 'Bandung', '1989-08-15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`pegawai_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `pegawai_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
